package com.luo.demo.sc.base.enums;

/**
 * 是否枚举
 *
 * @author luohq
 * @date 2022/01/30
 */
public enum YesOrNoEnum {
    YES(1, "是"),
    NO(2, "否");

    private Integer code;
    private String message;

    YesOrNoEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * code是否相等
     *
     * @param code
     * @return
     */
    public Boolean equalCode(Integer code) {
        for (YesOrNoEnum curEnum : values()) {
            if (curEnum.getCode().equals(code)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 根据code转换为枚举类
     *
     * @param code
     * @return
     */
    public static YesOrNoEnum ofCode(String code) {
        for (YesOrNoEnum curEnum : values()) {
            if (curEnum.getCode().equals(code)) {
                return curEnum;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
