package com.luo.demo.sc.base.model.param;

/**
 * 基础分页参数
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-01-25 14:20
 */
public class BasePageParam {
    private Integer pageNo = 1;
    private Integer pageSize = 10;
    private String orderBy;
    private String orderSort;

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getOrderSort() {
        return orderSort;
    }

    public void setOrderSort(String orderSort) {
        this.orderSort = orderSort;
    }

    @Override
    public String toString() {
        return "BasePageParam{" +
                "pageNo=" + pageNo +
                ", pageSize=" + pageSize +
                ", orderBy='" + orderBy + '\'' +
                ", orderSort='" + orderSort + '\'' +
                '}';
    }
}
