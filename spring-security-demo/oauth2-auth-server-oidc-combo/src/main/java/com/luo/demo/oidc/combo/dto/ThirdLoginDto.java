package com.luo.demo.oidc.combo.dto;

import lombok.Builder;
import lombok.Data;

/**
 * 三方登录信息 - DTO
 *
 * @author luohq
 * @date 2022-03-06 10:15
 */
@Data
@Builder
public class ThirdLoginDto {
    /**
     * 第三方ID
     */
    private String providerId;
    /**
     * 三方登录入口URI
     */
    private String loginUri;
    /**
     * 第三方图标
     */
    private String loginIcon;
}
