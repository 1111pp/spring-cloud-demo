package com.luo.demo.oidc.captcha.ext;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.impl.NoNoise;
import com.google.code.kaptcha.impl.ShadowGimpy;
import com.google.code.kaptcha.util.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

import static com.google.code.kaptcha.Constants.*;
 
/**
 * 谷歌验证码配置文件
 *
 * @author luohq
 * @date 2022-03-04
 * @version 1.0.0
 */
@Configuration
public class KaptchaConfig {

    /**
     * 字符验证码生成配置
     */
    @Bean(name = "captchaProducer")
    public DefaultKaptcha getKaptchaBean()
    {
        DefaultKaptcha defaultKaptcha = new DefaultKaptcha();
        Properties properties = new Properties();
        //设置图片边框
        properties.setProperty(KAPTCHA_BORDER, "no");
        //properties.setProperty(KAPTCHA_BORDER_COLOR, "105,179,90");
        properties.setProperty(KAPTCHA_BORDER_COLOR, "220,220,220");
        //properties.setProperty(KAPTCHA_BORDER, "no");
        //设置图片大小
        properties.setProperty(KAPTCHA_IMAGE_WIDTH, "100");
        properties.setProperty(KAPTCHA_IMAGE_HEIGHT, "40");
        //字符大小
        properties.setProperty(KAPTCHA_TEXTPRODUCER_FONT_SIZE, "20");
        //设置字符间距
        properties.setProperty(KAPTCHA_TEXTPRODUCER_CHAR_SPACE, "8");
        //设置字符颜色
        properties.setProperty(KAPTCHA_TEXTPRODUCER_FONT_COLOR, "blue");
        //验证码字符串长度（即包含多少个字符）
        properties.setProperty(KAPTCHA_TEXTPRODUCER_CHAR_LENGTH, "4");
        //验证码字符范围（如下为默认值）
        //properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_STRING, "abcde2345678gfynmnpwx");
        //设置字符字体
        properties.setProperty(KAPTCHA_TEXTPRODUCER_FONT_NAMES, "Arial,Courier");
        //设置噪音
        properties.setProperty(KAPTCHA_NOISE_COLOR, "white");
        properties.setProperty(KAPTCHA_NOISE_IMPL, NoNoise.class.getName());
        properties.setProperty(KAPTCHA_OBSCURIFICATOR_IMPL, ShadowGimpy.class.getName());
        //session key
        properties.setProperty(KAPTCHA_SESSION_CONFIG_KEY, KAPTCHA_SESSION_KEY);
        Config config = new Config(properties);
        defaultKaptcha.setConfig(config);
        return defaultKaptcha;
    }

    /**
     * 算术表达式验证码生成配置
     */
    @Bean(name = "captchaProducerMath")
    public DefaultKaptcha getKaptchaBeanMath()
    {
        DefaultKaptcha defaultKaptcha = new DefaultKaptcha();
        Properties properties = new Properties();
        //设置图片边框
        properties.setProperty(KAPTCHA_BORDER, "no");
        //properties.setProperty(KAPTCHA_BORDER, "yes");
        //properties.setProperty(KAPTCHA_BORDER_COLOR, "105,179,90");
        properties.setProperty(KAPTCHA_BORDER_COLOR, "220,220,220");
        //设置图片大小
        properties.setProperty(KAPTCHA_IMAGE_WIDTH, "100");
        properties.setProperty(KAPTCHA_IMAGE_HEIGHT, "40");
        //字符大小
        properties.setProperty(KAPTCHA_TEXTPRODUCER_FONT_SIZE, "20");
        //设置字符颜色
        properties.setProperty(KAPTCHA_TEXTPRODUCER_FONT_COLOR, "blue");
        //设置对应text生成器
        properties.setProperty(KAPTCHA_TEXTPRODUCER_IMPL, KaptchaMathExpTextCreator.class.getName());
        //设置字符间距
        properties.setProperty(KAPTCHA_TEXTPRODUCER_CHAR_SPACE, "5");
        //验证码字符串长度（即包含多少个字符）
        properties.setProperty(KAPTCHA_TEXTPRODUCER_CHAR_LENGTH, "6");
        //设置字符字体
        properties.setProperty(KAPTCHA_TEXTPRODUCER_FONT_NAMES, "Arial,Courier");
        //设置噪音
        properties.setProperty(KAPTCHA_NOISE_COLOR, "white");
        properties.setProperty(KAPTCHA_NOISE_IMPL, NoNoise.class.getName());
        properties.setProperty(KAPTCHA_OBSCURIFICATOR_IMPL, ShadowGimpy.class.getName());
        //session key
        properties.setProperty(KAPTCHA_SESSION_CONFIG_KEY, KAPTCHA_SESSION_KEY);
        Config config = new Config(properties);
        defaultKaptcha.setConfig(config);
        return defaultKaptcha;
    }
}
