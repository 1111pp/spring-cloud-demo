package com.luo.demo.security.authserver.handler.kapatcha;

import com.google.code.kaptcha.Constants;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Kaptcha 生成结果保存接口 - 默认Session保存实现
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-03-14 14:51
 */
public class DefaultKaptchaResultSaveServiceImpl implements KapatchResultSaveService {

    @Override
    public void saveResult(String resultText, HttpServletRequest request, HttpServletResponse response) {
        //获取session
        HttpSession session = request.getSession();
        //在session中设置验证码结果
        session.setAttribute(Constants.KAPTCHA_SESSION_KEY, resultText);
    }

    @Override
    public String getResult(HttpServletRequest request, HttpServletResponse response) {
        //获取session
        HttpSession session = request.getSession();
        //获取session中保存的验证码结果
        return (String) session.getAttribute(Constants.KAPTCHA_SESSION_KEY);
    }
}
