package com.luo.demo.security.controller;

import com.luo.demo.sc.base.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 页面controller
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-02-14 14:52
 */
@Controller
@Slf4j
public class PageController {

    @GetMapping("/")
    public String index(Model model, Authentication authentication) {
        log.info("main Authentication: {}", JsonUtils.toJson(authentication));
        String welcomeMsg = "Login Client1 - 欢迎你 - " + authentication.getName() + "!";
        log.info("cur welcomeMsg: {}", welcomeMsg);
        model.addAttribute("welcomeMsg", welcomeMsg);
        return "/main";
    }
}
