package com.luo.demo.security.controller;

import com.luo.demo.sc.base.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.security.oauth2.client.web.reactive.function.client.ServerOAuth2AuthorizedClientExchangeFilterFunction.clientRegistrationId;
import static org.springframework.security.oauth2.client.web.reactive.function.client.ServerOAuth2AuthorizedClientExchangeFilterFunction.oauth2AuthorizedClient;


/**
 * REST服务 - Controller
 *
 * @author luohq
 * @date 2022-02-18
 */
@RestController
@Slf4j
public class RestSvcController {

	@GetMapping("/hello")
	public String hello(Authentication authentication) {
		log.info("hello Authentication: {}", JsonUtils.toJson(authentication));
		String reply = "Reply Hello, " + authentication.getName() + "!";
		log.info("cur reply: {}", reply);
		return reply;
	}


	@GetMapping("/authInfo")
	public Map<String, Object> authInfo(Authentication authentication) {
		log.info("get Authentication: class={}", authentication.getClass());
		log.info("get Authentication: {}", JsonUtils.toJson(authentication));
		Map<String, Object> resultMap = new HashMap<>(2);

		OAuth2AuthenticationToken oAuth2AuthenticationToken = (OAuth2AuthenticationToken) authentication;
		DefaultOidcUser oidcUser = (DefaultOidcUser) oAuth2AuthenticationToken.getPrincipal();
		resultMap.put("idToken", oidcUser.getIdToken().getTokenValue());
		resultMap.put("oidcUserInfo", oidcUser.getUserInfo().getClaims());
		resultMap.put("oidc.claims", oidcUser.getClaims());
		return resultMap;
	}


	@Resource
	private WebClient webClient;

	@Value("${spring.security.oauth2.resourceserver.resource1:http://oauth2-resource1:8090/articles}")
	private String resourceUri;

	@Value("${spring.security.oauth2.authserver.resource1:http://oauth2-server:9000/api/articles}")
	private String authServerResourceUri;


	private final String clientRegistrationId = "luo-oauth2-client1";
	//private final String clientRegistrationId = "rbac-oauth2-client1";

	@GetMapping(value = "/articles1")
	public String[] getArticles(@RegisteredOAuth2AuthorizedClient(clientRegistrationId) OAuth2AuthorizedClient authorizedClient) {
		log.info("get articles, OAuth2AuthorizationClient: {}", JsonUtils.toJson(authorizedClient));
		String[] articles = this.webClient
				.get()
				.uri(this.resourceUri)
				.attributes(oauth2AuthorizedClient(authorizedClient))
				.retrieve()
				.bodyToMono(String[].class)
				.block();
		log.info("get articles1, result: {}", JsonUtils.toJson(articles));
		return articles;
	}

	@GetMapping(value = "/articles2")
	public String[] getArticles2() {
		String[] articles = this.webClient
				.get()
				.uri(this.resourceUri)
				.attributes(clientRegistrationId(clientRegistrationId))
				.retrieve()
				.bodyToMono(String[].class)
				.block();
		log.info("get articles2, result: {}", JsonUtils.toJson(articles));
		return articles;
	}

	@GetMapping(value = "/articles3")
	public String[] getArticles3() {
		String[] articles = this.webClient
				.get()
				.uri(this.authServerResourceUri)
				.attributes(clientRegistrationId(clientRegistrationId))
				.retrieve()
				.bodyToMono(String[].class)
				.block();
		log.info("get articles3 from oauth2 server, result: {}", JsonUtils.toJson(articles));
		return articles;
	}


	@GetMapping(value = "/articles4")
	public String[] getArticles4() {
		String[] articles = this.webClient
				.post()
				.uri(this.authServerResourceUri)
				.attributes(clientRegistrationId(clientRegistrationId))
				.retrieve()
				.bodyToMono(String[].class)
				.block();
		log.info("get articles3 from oauth2 server, result: {}", JsonUtils.toJson(articles));
		return articles;
	}


	@GetMapping(value = "/actions")
	public String getActions() {
		String actions = this.webClient
				.get()
				.uri("http://rabc-oauth2-server:8080/api/v1/actions")
				.attributes(clientRegistrationId(clientRegistrationId))
				.retrieve()
				.bodyToMono(String.class)
				.block();
		log.info("get actions, result: {}", JsonUtils.toJson(actions));
		return actions;
	}


}
