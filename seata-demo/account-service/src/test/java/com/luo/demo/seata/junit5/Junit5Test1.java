package com.luo.demo.seata.junit5;

import com.luo.demo.seata.base.BaseTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;

/**
 * Junit5 方法顺序测试
 *
 * @author luohq
 * @date 2021-12-18 10:48
 */
@Order(1)
//@TestClassOrder(ClassOrderer.OrderAnnotation.class)
//@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
class Junit5Test1 extends BaseTest {

    @Order(1)
    @Test
    void test1() {
        log.info("TEST_1_1");
    }

    @Order(2)
    @Test
    void test2() {
        log.info("TEST_1_2");
    }


    @Nested
    @Order(1)
    class NestedTests1 {

        @Test
        void test1() {
            log.info("TEST_1_NESTED_1");
        }
    }

    @Nested
    @Order(2)
    class NestedTests2 {

        @Test
        void test2() {
            log.info("TEST_1_NESTED_2");
        }
    }
}
