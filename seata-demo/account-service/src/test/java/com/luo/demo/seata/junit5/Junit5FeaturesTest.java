package com.luo.demo.seata.junit5;

import com.luo.demo.sc.base.execption.MsgRuntimeException;
import com.luo.demo.seata.base.BaseTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Duration;
import java.util.Arrays;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

/**
 * Junit5 特性测试
 *
 * @author luohq
 * @date 2021-12-18 10:48
 */
@Slf4j
class Junit5FeaturesTest extends BaseTest {


    @BeforeEach
    void setup() {
        log.info("beforeEach");
    }

    @AfterEach
    void teardown() {
        log.info("afterEach");
    }


    @Test
    void testJUnit5Assertions() {
        Integer result1 = 1;
        Integer result2 = 2;

        String[] strArray1 = {"A", "B", "C"};
        String[] strArray2 = {"A", "B", "C"};
        String[] strRegexArray = {"\\w", "B", "C"};

        /** assert格式：assertXXX(期望值, 实际值, 错误提示信息) */

        /** ========== 是否相等 ========== */
        //是否相等（通过equals判断）
        assertEquals(1, result1);
        assertNotEquals(2, result1);

        //为true或false
        assertTrue(() -> true);
        assertFalse(false);

        //是否为同一个对象引用（通过==判断）
        assertSame(result1, result1);
        assertNotSame(result1, result2);

        //数组是否相同
        assertArrayEquals(strArray1, strArray2);
        //iterable是否否相同（即collection，如List、set等）
        assertIterableEquals(Arrays.asList(strArray1), Arrays.asList(strArray2));
        //判断List<String>, Stream<String>是否相同或者正则匹配（或者fast-forward）
        assertLinesMatch(Arrays.asList(strRegexArray), Arrays.asList(strArray1));

        //是否是子类型
        Number num = assertInstanceOf(Number.class, result1);


        /** ========== 是否抛出异常 ========== */
        //抛出指定类型异常(抛出的异常是指定异常的子类型即可)
        RuntimeException runEx = assertThrows(RuntimeException.class, () -> {
            throw new MsgRuntimeException("测试异常");
        });
        assertEquals("测试异常", runEx.getMessage());

        //抛出指定类型异常(抛出的异常和指定异常的class类型必须完全相同)
        runEx = assertThrowsExactly(RuntimeException.class, () -> {
            throw new RuntimeException("测试异常");
        });
        assertEquals("测试异常", runEx.getMessage());

        //不抛出任何异常
        assertDoesNotThrow(() -> {
            log.info("test assertDoesNotThrow");
        });

        //所有的executables（在当前线程依次执行）均不抛出异常
        assertAll(() -> {
        }, () -> {
        }, () -> {
        });

        /** ========== 是否执行超时 ========== */
        //executable是否执行超时
        //在当前线程执行executable，在执行完成后计算超时，所以不会打断executable执行
        assertTimeout(Duration.ofSeconds(3), () -> {
            Thread.sleep(2000);
        });

        //executable是否执行超时
        //在不同线程执行executable，在executable执行算超后，可被抢占式abort
        assertTimeoutPreemptively(Duration.ofSeconds(3), () -> {
            Thread.sleep(2000);
        });

        /** ========== 手动触发失败 ========== */
        //fail("手动触发测试失败", new RuntimeException("手动触发测试失败异常"));
    }

    @Test
    void testJUnit5Assumptions() {
        Boolean isAllow = true;
        //假设条件为true，才继续执行（否则ignored）
        assumeTrue(isAllow, "assumeTrue...");
        //假设条件为false，才继续执行（否则ignored）
        assumeFalse(isAllow, "assumeFalse...");
        //条件为true则执行executable逻辑
        //相当于if条件成立则执行executable
        assumingThat(isAllow, () -> {
            System.out.println("assume execute...");
        });
    }

//    @NullSource
//    @EmptySource
//    @NullAndEmptySource
//    @MethodSource("stringProvider")
//    @CsvSource({
//     "1,2",
//     "3,4"
//    })
//    @CsvFileSource(resources = "/demo.csv", files = "src/test/resources/demo.csv")
//    @ArgumentsSource(MyArgumentsProvider.class)
    @ValueSource(strings = {"1", "2"})
    @ParameterizedTest
    void parameterizedTest(String arg) {
        log.info("ParameterizedTest: {}", arg);
    }

    @RepeatedTest(2)
    void repeatedTest() {
        log.info("RepeatedTest");
    }

    static Stream<String> stringProvider() {
        return Stream.of("apple", "banana");
    }

    static class MyArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
            return Stream.of("apple", "banana").map(Arguments::of);
        }
    }

}
