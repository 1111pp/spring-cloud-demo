package com.luo.demo.seata.controller;

import com.luo.demo.sc.base.enums.RespCodeEnum;
import com.luo.demo.sc.base.utils.JsonUtils;
import com.luo.demo.seata.base.BaseTest;
import com.luo.demo.seata.mapper.AccountMapper;
import com.luo.demo.seata.model.entity.Account;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import java.math.BigDecimal;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * 用户controller - 测试类
 *
 * @author luohq
 * @date 2021-12-11 11:18
 */
@Slf4j
class AccountControllerTest extends BaseTest {


    //@MockBean
    @SpyBean
    private AccountMapper accountMapper;


    /**
     * 用户扣款
     *
     * @throws Exception
     * @httpMethod post
     * @httpParam form
     * @mvcParam @RequestParam
     */
    @Test
    void debit() throws Exception {

        //录制mock操作
        //若使用@MockBean，则应使用when(targetObj.method(param...)).thenReturn(returnVal).thenThrow(ex)形式
        //when(this.accountMapper.debit(any(), any())).thenReturn(1);

        //录制spy操作
        //若使用@SpyBean，则应使用doReturn(returnVal).when(targetObj).method(param...)形式，
        //否则具体方法会被真实调用一次（引入未知问题，如null指针异常等）
        doReturn(1).when(this.accountMapper).debit(any(), any());


        this.mockMvc.perform(post("/account/debit")
                //post form参数
                .param("userId", "user-1")
                .param("money", "100.00"))
                //打印请求详细信息
                .andDo(print())
                //判断返回结果200
                .andExpect(status().isOk())
                //判断content-type兼容application/json
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                //判断响应体respCode为成功
                .andExpect(jsonPath("$.respCode").value(RespCodeEnum.SUCCESS.getCode()));
    }

    /**
     * 获取用户详情
     *
     * @throws Exception
     * @httpMethod get
     * @httpParam queryParam
     * @mvcParam @RequestParam
     */
    @Test
    void getDetail() throws Exception {

        Integer id = 1;
        //录制mock操作
        //when(this.accountMapper.selectById(any())).thenReturn(this.buildAccount(id, "user-100"));
        //录制spy操作
        doReturn(this.buildAccount(id, "user-100")).when(this.accountMapper).selectById(any());

        this.mockMvc.perform(get("/account/detail")
                //get queryParam参数
                .queryParam("id", id.toString()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.respCode").value(RespCodeEnum.SUCCESS.getCode()));

    }

    /**
     * 获取用户详情
     *
     * @throws Exception
     * @httpMethod get
     * @httpParam pathParam
     * @mvcParam @PathVariable
     */
    @Test
    void getDetailWithId() throws Exception {
        Integer id = 1;
        //录制mock操作
        //when(this.accountMapper.selectById(any())).thenReturn(this.buildAccount(id, "user-100"));
        //录制spy操作
        doReturn(this.buildAccount(id, "user-100")).when(this.accountMapper).selectById(any());

        //get pathParam参数
        this.mockMvc.perform(get("/account/detail/{id}", id))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.respCode").value(RespCodeEnum.SUCCESS.getCode()));

    }

    /**
     * 新增用户信息
     *
     * @throws Exception
     * @httpMethod post
     * @httpParam application/json
     * @mvcParam @RequestBody
     */
    @Test
    void addAccount() throws Exception {
        //请求体
        Account account = this.buildAccount(null, "user-100");
        String accountJson = JsonUtils.toJson(account);
        log.info("用户信息json: {}", accountJson);

        //录制mock操作
        //when(this.accountMapper.insert(any())).thenReturn(1);
        //录制spy操作
        doReturn(1).when(this.accountMapper).insert(any());


        MvcResult mvcResult = this.mockMvc.perform(post("/account/add")
                //application/json请求体
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(UTF_8)
                //具体请求Json参数
                .content(accountJson))
                .andDo(print())
                .andExpect(status().isOk())
                //由于在BaseTest setup中设置response.characterEncoding=UTF-8后，
                //此处实际返回MediaType.APPLICATION_JSON_UTF8，即application/json;charset=UTF-8
                //所以使用contentTypeCompatibleWith
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.respCode").value(RespCodeEnum.SUCCESS.getCode()))
                .andReturn();

        String resultJson = mvcResult.getResponse().getContentAsString();
        log.info("新增用户信息，结果：{}", resultJson);
    }

    /**
     * 新增用户信息
     *
     * @throws Exception
     * @httpMethod post
     * @httpParam application/json
     * @mvcParam @RequestBody
     */
    @Test
    void addAccountUtf8() throws Exception {
        Account account = this.buildAccount(null, "用户-100");
        String accountJson = JsonUtils.toJson(account);
        log.info("用户信息json: {}", accountJson);

        ///录制mock操作
        //when(this.accountMapper.insert(any())).thenReturn(1);
        //录制spy操作
        doReturn(1).when(this.accountMapper).insert(any());

        //设置post请求及参数
        ResultActions resultActions = this.mockMvc.perform(post("/account/add")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(UTF_8)
                .content(accountJson));
        //设置响应结果编码
        resultActions.andReturn().getResponse().setCharacterEncoding("UTF-8");
        //验证响应结果
        MvcResult mvcResult = resultActions.andDo(print())
                .andExpect(status().isOk())
                //设置完response.characterEncoding=UTF-8后，
                //此处实际返回MediaType.APPLICATION_JSON_UTF8，即application/json;charset=UTF-8
                //所以使用contentTypeCompatibleWith
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.respCode").value(RespCodeEnum.SUCCESS.getCode()))
                .andReturn();

        String resultJson = mvcResult.getResponse().getContentAsString();
        log.info("新增用户信息，结果：{}", resultJson);
    }


    private Account buildAccount(Integer id, String userId) {
        Account account = new Account();
        account.setId(id);
        account.setUserId(userId);
        account.setMoney(BigDecimal.valueOf(1000.0));
        String accountJson = JsonUtils.toJson(account);
        log.info("用户信息json: {}", accountJson);
        return account;
    }

}
