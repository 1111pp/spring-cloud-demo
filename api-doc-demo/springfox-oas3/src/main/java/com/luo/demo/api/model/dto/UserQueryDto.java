package com.luo.demo.api.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;

/**
 * 用户查询参数
 *
 * @author luohq
 * @date 2022-01-15 13:07
 */
@Data
@Schema(description = "用户查询参数")
public class UserQueryDto {
    /**
     * 用户ID
     */
    @Schema(description = "用户ID", example = "1", required = false)
    private Long id;
    /**
     * 用户名称
     */
    @Schema(description = "用户名称", example = "小明")
    @Size(min = 1, max = 30)
    private String name;
    /**
     * 用户性别（1:男，2:女）
     */
    @Schema(description = "用户性别（1:男，2:女）",  example = "1")
    @Range(min = 1, max = 2)
    private Integer sex;
    /**
     * 起始创建日期
     */
    @Schema(description = "起始创建日期", example = "2022-01-01 10:00:00")
    private LocalDateTime createTimeStart;
    /**
     * 结束创建日期
     */
    @Schema(description = "结束创建日期", example = "2022-01-16 08:20:08")
    private LocalDateTime createTimeEnd;


}
