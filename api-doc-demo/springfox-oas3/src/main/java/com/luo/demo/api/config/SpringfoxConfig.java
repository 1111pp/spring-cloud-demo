package com.luo.demo.api.config;

import com.luo.demo.sc.base.enums.RespCodeEnum;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Response;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Springfox配置
 *
 * @author luohq
 * @date 2022-01-15 17:33
 */
@Configuration
public class SpringfoxConfig {

    @Bean
    public Docket createRestApi() {
        List<Response> respMsgList = this.convertRespMsgList();
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .globalResponses(HttpMethod.GET, respMsgList)
                .globalResponses(HttpMethod.POST, respMsgList)
                .globalResponses(HttpMethod.PUT, respMsgList)
                .globalResponses(HttpMethod.DELETE, respMsgList)
                .select()
                // 为当前包路径
                .apis(RequestHandlerSelectors.basePackage("com.luo.demo.api.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean
    public ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                // 页面标题
                .title("Springfox OAS3.0 - RESTful API")
                // 创建人信息
                .contact(new Contact("luohq", "https://blog.csdn.net/luo15242208310", "luohengquan@yeah.com"))
                // 版本号
                .version("1.0")
                // 描述
                .description("Springfox OAS3.0 构建RESTful API" + this.convertRespMsgHtmlTable())
                .build();
    }

    /**
     * 转换响应码枚举RespCodeEnum为响应信息列表
     *
     * @return 响应信息列表
     */
    private List<Response> convertRespMsgList() {
        return Stream.of(RespCodeEnum.values())
                .map(respCodeEnum -> {
                    return new ResponseBuilder()
                            .code(respCodeEnum.getCode().toString())
                            .description(respCodeEnum.getMessage())
                            .build();
                }).collect(Collectors.toList());
    }

    /**
     * 转换通用响应码Table
     *
     * @return 响应码Table
     */
    private String convertRespMsgHtmlTable() {
        StringBuilder sb = new StringBuilder("<table><tr><th align=\"left\">响应码</th><th align=\"left\">提示信息</th></tr>");
        Stream.of(RespCodeEnum.values()).forEach(respCodeEnum -> {
            sb.append("<tr><td>")
                    .append(respCodeEnum.getCode())
                    .append("</td><td>")
                    .append(respCodeEnum.getMessage())
                    .append("</td></tr>");
        });
        return sb.append("</table>").toString();
    }
}
