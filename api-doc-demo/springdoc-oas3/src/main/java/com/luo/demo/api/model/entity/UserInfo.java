package com.luo.demo.api.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 用户信息
 *
 * @author luohq
 * @date 2022-01-15 12:21
 */
@Data
@Builder
@Schema(description = "用户信息")
public class UserInfo {
    /**
     * 用户ID
     */
    @Schema(description = "用户ID", required = true)
    private Long id;
    /**
     * 用户名称
     */
    @Schema(description = "用户名称", required = true)
    private String name;
    /**
     * 用户性别（1:男，2:女）
     */
    @Schema(description = "用户性别（1:男，2:女）", required = true, example = "1")
    private Integer sex;

    /**
     * 设备列表
     */
    @Schema(description = "设备列表", required = false)
    private List<DeviceInfo> deviceInfoList;
    /**
     * 创建时间
     */
    @Schema(description = "创建时间", example = "2022-01-16 18:00:03")
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    @JsonFormat(pattern = "yyyy年MM月dd日 HH:mm:ss")
    @Schema(description = "修改时间", example = "2022-01-16 18:00:03")
    private LocalDateTime updateTime;
}
