package com.luo.demo.api.model.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 设备信息
 *
 * @author luohq
 * @date 2022-01-15 12:23
 */
@Data
@Builder
@Schema(description = "设备信息")
public class DeviceInfo {
    /**
     * 设备ID
     */
    @Schema(description = "设备ID", required = true)
    private Long id;
    /**
     * 设备名称
     */
    @Schema(description = "设备名称", required = true)
    private String name;
    /**
     * 设备类型（1:电脑, 2:手机, 3:平板）
     */
    @Schema(description = "设备类型（1:电脑, 2:手机, 3:平板）", required = true)
    private Integer type;
    /**
     * 创建时间
     */
    @Schema(description = "创建时间", example = "2022-01-16 18:00:03")
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    @Schema(description = "修改时间", example = "2022-01-16 18:00:03")
    private LocalDateTime updateTime;
}
