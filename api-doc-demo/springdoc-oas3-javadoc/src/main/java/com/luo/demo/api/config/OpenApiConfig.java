package com.luo.demo.api.config;

import com.luo.demo.sc.base.enums.RespCodeEnum;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * OAS 3.0 配置
 *
 * @author luohq
 * @date 2022-01-16 12:34
 */
@Configuration
public class OpenApiConfig {
    @Bean
    public OpenAPI springShopOpenAPI() {
        return new OpenAPI()
                .info(new Info().title("Springdoc OAS3.0 Javadoc- RESTful API")
                        //.description("Springdoc OAS3.0结合Javadoc模块 构建RESTful API")// + this.convertRespMsgHtmlTable())
                        .version("1.0")
                        //.license(new License().name("Apache 2.0").url("http://springdoc.org"))
                        )
                .servers(Arrays.asList(
                        new Server().description("开发环境").url("http://localhost:8080")
                ));
                //.externalDocs(new ExternalDocumentation()
                //        .description("SpringShop Wiki Documentation")
                //        .url("https://springshop.wiki.github.org/docs"));
    }

    ///**
    // * 全局设置响应码（实际测试如果设置group（代码或配置文件）则此段代码不生效）
    // *
    // * @return
    // */
    //@Bean
    //public OperationCustomizer customizeOperation() {
    //    return (operation, handlerMethod) -> {
    //        System.out.println("op: " + operation.getSummary());
    //        ApiResponses curResponses = operation.getResponses();
    //        Stream.of(RespCodeEnum.values()).forEach(respCodeEnum -> {
    //            curResponses.addApiResponse(
    //                    String.valueOf(respCodeEnum.getCode()),
    //                    new ApiResponse().description(respCodeEnum.getMessage()));
    //        });
    //        return operation.responses(curResponses);
    //    };
    //}

    //代码配置分组（亦可直接通过配置文件进行配置springdoc.group-configs[*]）
    //@Bean
    //public GroupedOpenApi publicApi() {
    //    return GroupedOpenApi.builder()
    //            .group("用户管理")
    //            .pathsToMatch("/users/**")
    //            .build();
    //}
    //@Bean
    //public GroupedOpenApi adminApi() {
    //    return GroupedOpenApi.builder()
    //            .group("角色管理")
    //            .pathsToMatch("/roles/**")
    //            .build();
    //}


    /**
     * 转换通用响应码Table
     *
     * @return 响应码Table
     */
    private String convertRespMsgHtmlTable() {
        StringBuilder sb = new StringBuilder("<table><tr><th align=\"left\">响应码</th><th align=\"left\">提示信息</th></tr>");
        Stream.of(RespCodeEnum.values()).forEach(respCodeEnum -> {
            sb.append("<tr><td>")
                    .append(respCodeEnum.getCode())
                    .append("</td><td>")
                    .append(respCodeEnum.getMessage())
                    .append("</td></tr>");
        });
        return sb.append("</table>").toString();
    }
}
