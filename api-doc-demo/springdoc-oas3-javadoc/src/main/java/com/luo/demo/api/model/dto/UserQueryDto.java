package com.luo.demo.api.model.dto;

import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;

/**
 * 用户查询参数
 *
 * @author luohq
 * @date 2022-01-15 13:07
 */
public class UserQueryDto {
    /**
     * 用户ID
     */
    private Long id;
    /**
     * 用户名称
     */
    @Size(min = 1, max = 30)
    private String name;
    /**
     * 用户性别（1:男，2:女）
     */
    @Range(min = 1, max = 2)
    private Integer sex;
    /**
     * 起始创建日期
     */
    private LocalDateTime createTimeStart;
    /**
     * 结束创建日期
     */
    private LocalDateTime createTimeEnd;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public LocalDateTime getCreateTimeStart() {
        return createTimeStart;
    }

    public void setCreateTimeStart(LocalDateTime createTimeStart) {
        this.createTimeStart = createTimeStart;
    }

    public LocalDateTime getCreateTimeEnd() {
        return createTimeEnd;
    }

    public void setCreateTimeEnd(LocalDateTime createTimeEnd) {
        this.createTimeEnd = createTimeEnd;
    }

    @Override
    public String toString() {
        return "UserQueryDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex=" + sex +
                ", createTimeStart=" + createTimeStart +
                ", createTimeEnd=" + createTimeEnd +
                '}';
    }
}
