package com.luo.demo.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动应用
 *
 * @author luohq
 * @date 2022-01-15 12:01
 */
@SpringBootApplication
public class SpringdocOas3JavadocApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringdocOas3JavadocApplication.class, args);
    }
}
