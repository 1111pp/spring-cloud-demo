package com.luo.demo.api.model.entity;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 用户信息
 *
 * @author luohq
 * @date 2022-01-15 12:21
 */
public class UserInfo {
    /**
     * 用户ID
     */
    private Long id;
    /**
     * 用户名称
     */
    private String name;
    /**
     * 用户性别（1:男，2:女）
     */
    private Integer sex;
    /**
     * 设备列表
     */
    private List<DeviceInfo> deviceInfoList;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    private UserInfo(Builder builder) {
        setId(builder.id);
        setName(builder.name);
        setSex(builder.sex);
        setDeviceInfoList(builder.deviceInfoList);
        setCreateTime(builder.createTime);
        setUpdateTime(builder.updateTime);
    }

    public static Builder builder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public List<DeviceInfo> getDeviceInfoList() {
        return deviceInfoList;
    }

    public void setDeviceInfoList(List<DeviceInfo> deviceInfoList) {
        this.deviceInfoList = deviceInfoList;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex=" + sex +
                ", deviceInfoList=" + deviceInfoList +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }


    public static final class Builder {
        private Long id;
        private String name;
        private Integer sex;
        private List<DeviceInfo> deviceInfoList;
        private LocalDateTime createTime;
        private LocalDateTime updateTime;

        private Builder() {
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder sex(Integer val) {
            sex = val;
            return this;
        }

        public Builder deviceInfoList(List<DeviceInfo> val) {
            deviceInfoList = val;
            return this;
        }

        public Builder createTime(LocalDateTime val) {
            createTime = val;
            return this;
        }

        public Builder updateTime(LocalDateTime val) {
            updateTime = val;
            return this;
        }

        public UserInfo build() {
            return new UserInfo(this);
        }
    }
}
