package com.luo.demo.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动应用
 *
 * @author luohq
 * @date 2022-01-15 12:01
 */
@SpringBootApplication
public class SmartdocOas3Application {
    public static void main(String[] args) {
        SpringApplication.run(SmartdocOas3Application.class, args);
    }
}
