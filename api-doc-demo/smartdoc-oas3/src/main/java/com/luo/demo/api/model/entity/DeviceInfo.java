package com.luo.demo.api.model.entity;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 设备信息
 *
 * @author luohq
 * @date 2022-01-15 12:23
 */
@Data
@Builder
public class DeviceInfo {
    /**
     * 设备ID
     */
    private Long id;
    /**
     * 设备名称
     */
    private String name;
    /**
     * 设备类型（1:电脑, 2:手机, 3:平板）
     */
    private Integer type;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    private LocalDateTime updateTime;
}
