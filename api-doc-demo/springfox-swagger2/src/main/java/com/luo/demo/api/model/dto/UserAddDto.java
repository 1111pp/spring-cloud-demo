package com.luo.demo.api.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * 新增用户
 *
 * @author luohq
 * @date 2022-01-15 12:21
 */
@Data
@Builder
@ApiModel("新增用户参数")
public class UserAddDto {
    /**
     * 用户名称
     */
    @ApiModelProperty(value = "用户名称")
    @NotBlank
    @Size(min = 1, max = 30)
    private String name;
    /**
     * 用户性别（1:男，2:女）
     */
    @ApiModelProperty(value = "用户性别（1:男，2:女）")
    @NotNull
    @Range(min = 1, max = 2)
    private Integer sex;

    /**
     * 设备列表
     */
    @ApiModelProperty(value = "设备列表")
    @NotEmpty
    private List<DeviceAddDto> deviceInfoList;
}
