package com.luo.demo.api.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 用户信息
 *
 * @author luohq
 * @date 2022-01-15 12:21
 */
@Data
@Builder
@ApiModel("用户信息")
public class UserInfo {
    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID", required = true)
    private Long id;
    /**
     * 用户名称
     */
    @ApiModelProperty(value = "用户名称", required = true)
    private String name;
    /**
     * 用户性别（1:男，2:女）
     */
    @ApiModelProperty(value = "用户性别（1:男，2:女）", required = true)
    private Integer sex;

    /**
     * 设备列表
     */
    @ApiModelProperty(value = "设备列表", required = false)
    private List<DeviceInfo> deviceInfoList;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;
}
