package com.luo.demo.api.config;

import com.luo.demo.sc.base.enums.RespCodeEnum;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Springfox配置
 *
 * @author luohq
 * @date 2022-01-15 17:33
 */
@Configuration
@EnableSwagger2
public class SpringfoxConfig {

    @Bean
    public Docket createRestApi() {
        List<ResponseMessage> respMsgList = this.convertRespMsgList();
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .globalResponseMessage(RequestMethod.GET, respMsgList)
                .globalResponseMessage(RequestMethod.POST, respMsgList)
                .globalResponseMessage(RequestMethod.PUT, respMsgList)
                .globalResponseMessage(RequestMethod.DELETE, respMsgList)
                .select()
                // 为当前包路径
                .apis(RequestHandlerSelectors.basePackage("com.luo.demo.api.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean
    public ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                // 页面标题
                .title("Springfox Swagger2 - RESTful API")
                // 创建人信息
                .contact(new Contact("luohq", "https://blog.csdn.net/luo15242208310", "luohengquan@yeah.com"))
                // 版本号
                .version("1.0")
                // 描述
                .description("Springfox Swagger2构建RESTful API")
                .build();
    }

    /**
     * 转换响应码枚举RespCodeEnum为响应信息列表
     *
     * @return 响应信息列表
     */
    private List<ResponseMessage> convertRespMsgList() {
        return Stream.of(RespCodeEnum.values())
                .map(respCodeEnum -> {
                    return new ResponseMessageBuilder()
                            .code(respCodeEnum.getCode())
                            .message(respCodeEnum.getMessage())
                            .build();
                }).collect(Collectors.toList());
    }
}
