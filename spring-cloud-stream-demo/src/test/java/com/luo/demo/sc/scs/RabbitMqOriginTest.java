package com.luo.demo.sc.scs;

import com.luo.demo.sc.base.enums.RespCodeEnum;
import com.luo.demo.sc.base.test.BaseTest;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Rabbitmq - 测试类
 *
 * @author luohq
 * @date 2021-12-22 09:39
 */
public class RabbitMqOriginTest extends BaseTest {

    @Profile("rabbitmq-origin")
    @ParameterizedTest
    @CsvSource({
            "exchange1, mykey, hello1",
            "exchange2, mykey, hello2"}
    )
    void sendRabbitMsg(String exchange, String routingKey, String msg) throws Exception {
        this.mockMvc.perform(post("/rabbit/send/{exchange}/{routingKey}/{msg}", exchange, routingKey, msg))
                .andDo(print())
                //判断返回结果200
                .andExpect(status().isOk())
                //判断content-type兼容application/json
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                //判断响应体respCode为成功
                .andExpect(jsonPath("$.respCode").value(RespCodeEnum.SUCCESS.getCode()));
    }
}
