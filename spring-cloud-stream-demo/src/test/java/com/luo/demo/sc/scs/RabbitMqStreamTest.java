package com.luo.demo.sc.scs;

import com.luo.demo.sc.base.enums.RespCodeEnum;
import com.luo.demo.sc.base.test.BaseTest;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Rabbitmq - 测试类
 *
 * @author luohq
 * @date 2021-12-22 09:39
 */
public class RabbitMqStreamTest extends BaseTest {

    @Profile("rabbitmq-scs")
    @ParameterizedTest
    @ValueSource(strings = {"biz1Producer-out-0", "biz2Producer-out-0"})
    void sendRabbitMsg(String bizName) throws Exception {
        this.mockMvc.perform(post("/rabbit/send/{bizName}/{msg}", bizName, "hello"))
                .andDo(print())
                //判断返回结果200
                .andExpect(status().isOk())
                //判断content-type兼容application/json
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                //判断响应体respCode为成功
                .andExpect(jsonPath("$.respCode").value(RespCodeEnum.SUCCESS.getCode()));
    }
}
