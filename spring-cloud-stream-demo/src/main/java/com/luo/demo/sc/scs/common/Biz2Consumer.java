package com.luo.demo.sc.scs.common;

import lombok.extern.slf4j.Slf4j;

import java.util.function.Consumer;

/**
 * biz2对应的消费者
 *
 * @author luohq
 * @date 2021-12-21 15:48
 */
@Slf4j
public class Biz2Consumer implements Consumer<String> {

    @Override
    public void accept(String message) {
       log.info("[biz2Consumer] RECV MSG: {}", message);
    }
}
