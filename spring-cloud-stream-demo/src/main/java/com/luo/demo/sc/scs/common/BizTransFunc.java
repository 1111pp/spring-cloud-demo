package com.luo.demo.sc.scs.common;

import lombok.extern.slf4j.Slf4j;

import java.util.function.Function;

/**
 * 接收消息、转发消息 - function
 *
 * @author luohq
 * @date 2021-12-22 10:01
 */
@Slf4j
public class BizTransFunc implements Function<String, String> {

    @Override
    public String apply(String message) {
        log.info("[bizTrans] RECV MSG: {}", message);
        String transMessage = String.format("from biz1 to biz2: %s", message);
        log.info("[bizTrans] SEND MSG: {}", transMessage);
        return transMessage;
    }
}
