package com.luo.demo.sc.scs.rabbitmq;

import com.luo.demo.sc.base.model.result.RespResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * rabbitmq - 原生示例
 *
 * @author luohq
 * @date 2021-12-22 11:24
 */
@Profile("rabbitmq-origin")
@Configuration
@Slf4j
@Controller
public class RabbitMqOrigin {

    /**
     * rabbitMq模板（发送消息）
     */
    @Resource
    private RabbitTemplate rabbitTemplate;


    /**
     * 定义biz1消息接收者
     *
     * @param message
     * @rabbit.exhange exchange1
     * @rabbit.aueue queue1
     * @rabbit.bindingKey #
     */
    @RabbitListener(
            id = "biz1Consumer",
            bindings = @QueueBinding(
                    value = @Queue(name = "${spring.rabbitmq.biz1.queue}", durable = "true", autoDelete = "false", exclusive = "false"),
                    exchange = @Exchange(name = "${spring.rabbitmq.biz1.exchange}", type = "topic", durable = "true", autoDelete = "false"),
                    key = "${spring.rabbitmq.biz1.binding-key}")
    )
    public void biz1Consumer(String message) {
        log.info("[biz1Consumer] RECV MSG: {}", message);
    }


    /**
     * 定义biz2消息接收者
     *
     * @param message
     * @rabbit.exhange exchange2
     * @rabbit.aueue queue2
     * @rabbit.bindingKey #
     */
    @RabbitListener(
            id = "biz2Consumer",
            bindings = @QueueBinding(
                    value = @Queue(name = "${spring.rabbitmq.biz2.queue}", durable = "true", autoDelete = "false", exclusive = "false"),
                    exchange = @Exchange(name = "${spring.rabbitmq.biz2.exchange}", type = "topic", durable = "true", autoDelete = "false"),
                    key = "${spring.rabbitmq.biz2.binding-key}")
    )
    public void biz2Consumer(String message) {
        log.info("[biz2Consumer] RECV MSG: {}", message);
    }


    /**
     * 发送消息
     *
     * @param exchange   rabbitmq交换器
     * @param routingKey 路由key
     * @param msg        消息内容
     * @return 发送结果
     */
    @PostMapping("/rabbit/send/{exchange}/{routingKey}/{msg}")
    @ResponseBody
    public RespResult sendMsg(@PathVariable String exchange, @PathVariable String routingKey, @PathVariable String msg) {
        try {
            log.info("发送rabbitMq消息：exchange={}, routingKey={}, msg={}", exchange, routingKey, msg);
            this.rabbitTemplate.convertAndSend(exchange, routingKey, msg);
            return RespResult.success();
        } catch (Throwable ex) {
            log.error("发送rabbitMq消息 - 异常！", ex);
            return RespResult.failed();
        }
    }


}
